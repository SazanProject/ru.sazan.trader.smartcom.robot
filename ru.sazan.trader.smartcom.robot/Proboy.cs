﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Configuration;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Utility;
using ru.sazan.trader.smartcom.Commands;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.smartcom.Handlers;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Inputs;
using ru.sazan.trader.Handlers.Spreads;
using SmartCOM3Lib;
using System.Reflection;
using System.Diagnostics;
using ru.sazan.trader.Handlers.StopLoss;
using ru.sazan.trader.Handlers.TakeProfit;

namespace ru.sazan.trader.smartcom.robot
{
    class Proboy
    {
        private static MarketDataProvider marketDataProvider = 
            new MarketDataProvider();

        private static RawTradingDataProvider rawTradingDataProvider = 
            new RawTradingDataProvider(DefaultLogger.Instance);

        private static SymbolsDataProvider symbolsDataProvider = 
            new SymbolsDataProvider();

        private static TraderBase traderBase =
            new TraderBase(new SmartComOrderManager());

        private static SmartComAdapter adapter = 
            new SmartComAdapter();
        
        private static Strategy strategy = 
            new Strategy(1, 
                "Proboy take profit and stop loss points strategy",
                AppSettings.GetStringValue("Portfolio"),
                AppSettings.GetStringValue("Symbol"),
                AppSettings.GetValue<double>("Amount"));
        
        private static BarSettings barSettings = new BarSettings(strategy,
            strategy.Symbol,
            AppSettings.GetValue<int>("Interval"),
            AppSettings.GetValue<int>("Period"));

        private static ProfitPointsSettings ppSettings =
            new ProfitPointsSettings(strategy, AppSettings.GetValue<double>("ProfitPoints"), false);

        private static TakeProfitOrderSettings poSettings =
            new TakeProfitOrderSettings(strategy, 86400);

        private static StopPointsSettings spSettings =
            new StopPointsSettings(strategy, AppSettings.GetValue<double>("StopPoints"), false);

        private static StopLossOrderSettings soSettings =
            new StopLossOrderSettings(strategy, 86400);

        private static string[] assemblies = { "Interop.SmartCOM3Lib.dll", "ru.sazan.trader.dll", "ru.sazan.trader.smartcom.dll", "ru.sazan.trader.smartcom.robot.exe" };

        static void Main(string[] args)
        {
            LogAssemblyInfo();

            AddStrategySettings();

            StrategiesPlaceStopLossByPointsOnTradeHandlers stopLossOnTradeHandlers =
                new StrategiesPlaceStopLossByPointsOnTradeHandlers(TradingData.Instance, 
                    SignalQueue.Instance, 
                    DefaultLogger.Instance, 
                    AppSettings.GetValue<bool>("MeasureStopFromSignalPrice"));

            StrategiesStopLossByPointsOnTickHandlers stopLossOnTickHandlers =
                new StrategiesStopLossByPointsOnTickHandlers(TradingData.Instance,
                    SignalQueue.Instance,
                    DefaultLogger.Instance,
                    AppSettings.GetValue<bool>("MeasureStopFromSignalPrice"));

            StrategiesPlaceTakeProfitByPointsOnTradeHandlers takeProfitOnTradeHandlers =
                new StrategiesPlaceTakeProfitByPointsOnTradeHandlers(TradingData.Instance,
                    SignalQueue.Instance,
                    DefaultLogger.Instance,
                    AppSettings.GetValue<bool>("MeasureProfitFromSignalPrice"));

            StrategiesTakeProfitByPointsOnTickHandlers takeProfitOnTickHandlers =
                new StrategiesTakeProfitByPointsOnTickHandlers(TradingData.Instance,
                    SignalQueue.Instance,
                    DefaultLogger.Instance,
                    AppSettings.GetValue<bool>("MeasureProfitFromSignalPrice"));
               
            SmartComHandlers.Instance.Add<_IStClient_DisconnectedEventHandler>(ScalperIsDisconnected);
            SmartComHandlers.Instance.Add<_IStClient_ConnectedEventHandler>(ScalperIsConnected);

            BreakOutOnTick openHandler =
                new BreakOutOnTick(strategy,
                    TradingData.Instance,
                    SignalQueue.Instance,
                    DefaultLogger.Instance);

            UpdateBarsOnTick updateBarsHandler =
                new UpdateBarsOnTick(barSettings,
                    new TimeTracker(),
                    TradingData.Instance,
                    DefaultLogger.Instance);

            AddStrategySubscriptions();

            adapter.Start();

            while (true)
            {
                try
                {
                    string command = Console.ReadLine();

                    if (command == "x")
                    {
                        adapter.Stop();

                        ExportData<Order>(AppSettings.GetValue<bool>("ExportOrdersOnExit"));
                        ExportData<Trade>(AppSettings.GetValue<bool>("ExportTradesOnExit"));

                        break;
                    }

                    if (command == "p")
                    {
                        Console.Clear();

                        Console.WriteLine(String.Format("Реализованный профит и лосс составляет {0} пунктов",
                            TradingData.Instance.GetProfitAndLossPoints(strategy)));
                    }

                    if (command == "t")
                    {
                        Console.Clear();

                        foreach (Trade item in TradingData.Instance.Get<IEnumerable<Trade>>())
                            Console.WriteLine(item.ToString());
                    }

                    if (command == "b")
                    {
                        Console.Clear();

                        foreach (Bar item in TradingData.Instance.Get<IEnumerable<Bar>>())
                            Console.WriteLine(item.ToString());
                    }

                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    DefaultLogger.Instance.Log(e.Message);

                    adapter.Restart();
                }
            }
        }

        private static void AddStrategySettings()
        {
            TradingData.Instance.Get<ICollection<Strategy>>().Add(strategy);
            TradingData.Instance.Get<ICollection<BarSettings>>().Add(barSettings);
            TradingData.Instance.Get<ICollection<ProfitPointsSettings>>().Add(ppSettings);
            TradingData.Instance.Get<ICollection<TakeProfitOrderSettings>>().Add(poSettings);
            TradingData.Instance.Get<ICollection<StopPointsSettings>>().Add(spSettings);
            TradingData.Instance.Get<ICollection<StopLossOrderSettings>>().Add(soSettings);
        }

        private static void AddStrategySubscriptions()
        {
            DefaultSubscriber.Instance.Portfolios.Add(strategy.Portfolio);
            DefaultSubscriber.Instance.BidsAndAsks.Add(strategy.Symbol);
            DefaultSubscriber.Instance.Ticks.Add(strategy.Symbol);
        }

        private static void ScalperIsDisconnected(string reason)
        {
            DefaultLogger.Instance.Log("Cleaning Bar collection.");
            TradingData.Instance.Get<ICollection<Bar>>().Clear();
        }

        private static void ScalperIsConnected()
        {
            DefaultLogger.Instance.Log("Requesting history bars.");
            Transaction getBars = new GetBarsCommand(barSettings.Symbol, barSettings.Interval, barSettings.Period);
            getBars.Execute();
        }

        private static void LogAssemblyInfo()
        {

            foreach (string assembly in assemblies) 
            { 
                DefaultLogger.Instance.Log(
                    String.Format("assembly: {0} version: {1}", 
                    assembly, 
                    FileVersionInfo.GetVersionInfo(assembly).ProductVersion));
            }
        }

        private static void ExportData<T>(bool confirmExport)
        {
            if (!confirmExport)
                return;

            string prefix = typeof(T).Name;

            Logger logger = new TextFileLogger(prefix, 10000000);

            foreach (T item in TradingData.Instance.Get<IEnumerable<T>>())
                logger.Log(item.ToString());
        }
    }
}
